\section{Метрические пространства}
	\begin{definition*}
		Метрическим пространством называется пара $(X, \rho)$, где $X$ --- некоторое множество, $\rho$ --- отображение $X \times X \to \real$, называемое метрикой, такое, что для любых $x, y, z \in X$ выполнено:
		\begin{enumerate}[label=M\arabic*.]
			\item $\rho(x, y) \geq 0$,
			\item $\rho(x, y) = 0$ тогда и только тогда, когда $x = y$,
			\item $\rho(x, y) = \rho(y, x)$,
			\item $\rho(x, y) \leq \rho(x, z) + \rho(z, y)$.
		\end{enumerate}
	\end{definition*}
	В дальнейшем, если метрика $\rho$ понятна из контекста, будем говорить просто <<метрическое пространство $X$>>.
	\begin{definition*}
		Последовательность $\{x_n\}_{n = 0}^\infty$ элементов метрического пространства $X$, сходится к $x \in X$ ($x_n \xrightarrow[n \to \infty]{} x$), если $\rho(x_n, x) \xrightarrow[n \to \infty]{} 0$.
	\end{definition*}
	\begin{definition*}
		Последовательность $\{x_n\}_{n = 0}^\infty$ называется фундаментальной (последовательностью Коши), если для любого $\varepsilon > 0$ найдётся $N > 0$, что для всех $m, n \geq N$ выполняется $\rho(x_n, x_m) < \varepsilon$.
	\end{definition*}
		Нетрудно проверить, что любая сходящаяся последовательность является фундаментальной, однако может оказаться, что не всякая фундаментальная последовательность сходится.
	\begin{definition*}
		Метрическое пространство $X$ называется полным, если любая фундаментальная последовательность в $X$ сходится к некоторому элементу этого пространства.
	\end{definition*}
	\begin{example*}\
		\begin{enumerate}
			\item $X = C[0, 1]$, $\rho_1(x, y) = \max_{t \in [0, 1]} \abs{x(t) - y(t)}$\footnote{максимум существует в силу теоремы Вейерштрасса}. Проверим, что $\rho_1$ действительно является метрикой, т.е. выполнено M1--M4. Ясно, что $\rho_1$ удовлетворяется свойствам M1--M3, а так как для любого $t \in [0, 1]$
			\begin{align}
				\abs{x(t) - y(t)} &\leq \abs{x(t) - z(t)} + \abs{z(t) - y(t)},\label{eq:1-1}\\
				\intertext{то}
				\rho_1(x, y)
				= \max_{t \in [0, 1]} \abs{x(t) - y(t)}
				&\leq \max_{t \in [0, 1]} \left[\abs{x(t) - z(t)} + \abs{z(t) - y(t)} \right]\nonumber\\
				&\leq \max_{t \in [0, 1]} \abs{x(t) - z(t)} + \max_{t \in [0, 1]} \abs{z(t) - y(t)}\nonumber\\
				&= \rho_1(x, z) + \rho_1(z, y)\nonumber,
			\end{align}
			то есть M4 также выполнено.
			\item $X = C[0, 1]$, $\rho_2(x, y) = \int_0^1 \abs{x(t) - y(t)} \diff t$. Выполнение M1 и M3 очевидно, M2 следует из известного утверждения о том, что если $f$ --- неотрицательная непрерывная функция и $\int_0^1 f(t) \diff t = 0$, то $f \equiv 0$ на $[0, 1]$; M4 проверяется из \eqref{eq:1-1} и аддитивности интеграла.
			
			Отметим, что сходимость и фундаментальность последовательностей зависит от используемой метрики. Рассмотрим последовательность функций $x_n(t) = t^n$ на $[0, 1]$. Несложно проверить\footnote{например, при $m = 2n$ имеем $\rho_1(x_n, x_{2n}) = \frac{1}{4}$}, что
			\begin{equation*}
				\rho_1(x_n, x_m) = \max_{t \in [0, 1]} \abs{t^n - t^m} \centernot{\xrightarrow[n, m \to \infty]{}} 0,
			\end{equation*}
			и последовательность $\{x_n\}_{n = 1}^\infty$ не является фундаментальной в $(X, \rho_1)$. В то же время,
			\begin{equation*}
				\rho_2(x_n, x_m) = \int_0^1 \abs{t^n - t^m} \diff t = \abs{\dfrac{1}{n + 1} - \dfrac{1}{m + 1}} \xrightarrow[n, m \to \infty]{} 0,
			\end{equation*}
			то есть в пространстве $(X, \rho_2)$ рассматриваемая последовательность фундаментальна.
			\item $X = S[a, b]$ --- пространство измеримых по Лебегу функций на $[a, b]$ с метрикой
			\begin{equation*}
				\rho(x, y) = \int_0^1 \dfrac{\abs{x(t) - y(t)}}{1 + \abs{x(t) - y(t)}} \diff t.
			\end{equation*}
			Выполнение M1 и M3 очевидно; $\rho(x, y) = 0$ тогда и только тогда, когда $x = y$ почти всюду, откуда следует M2 (со стандартным предположением считать одинаковыми совпадающие п.в. функции), для доказательства M4 сначала заметим, что функция
			\begin{equation*}
				\varphi(\lambda) = \dfrac{\lambda}{1 + \lambda} = 1 - \dfrac{1}{1 + \lambda}
			\end{equation*}
			является монотонно возрастающей по $\lambda$, откуда
			\begin{align*}
				\dfrac{\abs{x(t) - y(t)}}{1 + \abs{x(t) - y(t)}}
				&\leq \dfrac{\abs{x(t) - z(t)} + \abs{z(t) - y(t)}}{1 + \abs{x(t) - y(t)} + \abs{z(t) - y(t)}}\\
				&= \dfrac{\abs{x(t) - z(t)}}{1 + \abs{x(t) - y(t)} + \abs{z(t) - y(t)}} + \dfrac{\abs{z(t) - y(t)}}{1 + \abs{x(t) - y(t)} + \abs{z(t) - y(t)}}\\
				&\leq \dfrac{\abs{x(t) - z(t)}}{1 + \abs{x(t) - y(t)}} + \dfrac{\abs{z(t) - y(t)}}{1 + \abs{z(t) - y(t)}}.
			\end{align*}
			Интегрируя полученное неравенство, получим требуемое\footnote{стоит отметить, что сходимость по метрике в $(S, \rho)$ эквивалентна сходимости по мере}.
		\end{enumerate}
	\end{example*}